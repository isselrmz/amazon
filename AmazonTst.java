package NewPkg;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.lang.*;

//comment the above line and uncomment below line to use Chrome

public class AmazonTst {
public static void main(String[] args) throws InterruptedException {
// declaration and instantiation of objects/variables
System.setProperty("webdriver.chrome.driver","/Users/kmpw648/eclipse-workspace/newproject/chromedriver");

//comment the above 2 lines and uncomment below 2 lines to use Chrome
//System.setProperty("webdriver.chrome.driver","G:\\chromedriver.exe");
WebDriver driver = new ChromeDriver();
driver.manage().window().maximize();
String Url = "http://www.amazon.com.mx";
String Cart = "https://www.amazon.com.mx/gp/cart/view.html?ref_=nav_cart";
// launch Fire fox and direct it to the Base URL
driver.get(Url);
//driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]"));
driver.findElement(By.xpath("//*[@id=\"twotabsearchtextbox\"]")).sendKeys("SmartTV");
driver.findElement(By.xpath("//*[@id=\"nav-search-submit-button\"]")).click();

//Element visible
driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[10]/div/span/div/div/div[2]/div[1]/h2/a/span")).isDisplayed();
driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[10]/div/span/div/div/div[2]/div[1]/h2/a/span")).click();
//Add to Cart
driver.findElement(By.xpath("//*[@id=\"add-to-cart-button\"]")).click();
Thread.sleep(2000);
driver.get(Cart);

//Click on buy
driver.findElement(By.xpath("//*[@id=\"sc-buy-box-ptc-button\"]/span/input")).click();

//Create amazon account Elements//
By MailBx = By.xpath("//*[@id=\"ap_email\"]");
By Continue = By.xpath("//*[@id=\"continue\"]");
By Pwd = By.xpath("//*[@id=\"ap_password\"]");
By Login = By.xpath("//*[@id=\"signInSubmit\"]");

//Script
driver.findElement(MailBx).sendKeys("rulas_12@hotmail.com");
driver.findElement(Continue).click();
driver.findElement(Pwd).sendKeys("sfdc@123");
driver.findElement(Login).click();

}
}